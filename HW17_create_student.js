"use strict";

const student = {

	firstName: this.firstName,
	lastName: this.lastName,
	tabel: {},

	academicPerformance() {
		let i = 0;
		for (const subject in this.tabel) {
			(this.tabel[subject] <= 4) && i++;
		}
		return !i;
	},

	averageGrade() {
		let sum = 0;
		let i = 0;
		for (const subject in this.tabel) {
			sum += +this.tabel[subject];
			i++;
		}
		return (sum / i).toFixed(2);
	}
}

function askSubject(obj) {

	const defaultSubjects = ['історія', 'біологія', 'математика', 'фізика', 'географія'];
	let i = 0;
	
	while (true) {
		const subject = prompt("Введіть назву предмету", defaultSubjects[i] || '');
		const grade = prompt("Введіть оцінку по 12-бальній системі");

		if (!subject || !grade) break;

		if (!/^([0-9]|1[0-2])$/g.test(grade) || !/\d/g.test(grade) || /\d/g.test(subject)) {
			console.log("Ви не корректно вказали назву або оцінку останнього предмету");
			break;
		}

		obj.tabel[subject] = grade;
		i++;
	}
};

student.firstName = prompt("Введіть ваше ім'я", "Ivan");
student.lastName = prompt("Введіть ваше прізвище", "Petrenko");

askSubject(student);

console.log(student);
console.log(student.academicPerformance() ? `Студента ${student.firstName} ${student.lastName} переведено на наступний курс!` : `Треба краще вчитись!`);
const grade = student.averageGrade();
console.log(grade > 7 ? `Студенту призначено стипендію! Ваш середный бал ${grade}` : `Ваш середный бал ${grade}`);